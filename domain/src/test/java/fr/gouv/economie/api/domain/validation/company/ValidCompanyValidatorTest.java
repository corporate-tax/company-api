package fr.gouv.economie.api.domain.validation.company;

import fr.gouv.economie.api.domain.model.Company;
import fr.gouv.economie.api.domain.model.CompanyType;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class ValidCompanyValidatorTest {

    private final ValidCompanyValidator validator = new ValidCompanyValidator();

    @Test
    void isValid_valid_noAddress() {
        assertTrue(validator.isValid(new Company().setType(CompanyType.AUTO_ENTREPRISE), mock(ConstraintValidatorContext.class)));
    }

    @Test
    void isValid_valid_withAddress() {
        assertTrue(validator.isValid(new Company().setType(CompanyType.SAS).setAddress("address"), mock(ConstraintValidatorContext.class)));
    }

    @Test
    void isValid_not_valid_noAddress() {
        assertFalse(validator.isValid(new Company().setType(CompanyType.AUTO_ENTREPRISE).setAddress("address"), mock(ConstraintValidatorContext.class)));
    }

    @Test
    void isValid_not_valid_withAddress() {
        assertFalse(validator.isValid(new Company().setType(CompanyType.SAS), mock(ConstraintValidatorContext.class)));
    }
}