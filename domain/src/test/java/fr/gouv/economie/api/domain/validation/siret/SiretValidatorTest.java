package fr.gouv.economie.api.domain.validation.siret;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class SiretValidatorTest {

    private final SiretValidator validator = new SiretValidator();

    @Test
    void isValid_valid() {
        assertTrue(validator.isValid("44372668776223", mock(ConstraintValidatorContext.class)));
    }

    @Test
    void isValid_not_valid() {
        assertFalse(validator.isValid("44372668776224", mock(ConstraintValidatorContext.class)));
    }
}