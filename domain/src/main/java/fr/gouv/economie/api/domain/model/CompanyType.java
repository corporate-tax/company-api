package fr.gouv.economie.api.domain.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum CompanyType {

    AUTO_ENTREPRISE(false),
    SAS(true);

    private boolean hasAddress;

    public boolean hasAddress() {
        return hasAddress;
    }
}
