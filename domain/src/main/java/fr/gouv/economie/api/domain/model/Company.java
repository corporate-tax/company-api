package fr.gouv.economie.api.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode
@Accessors(chain = true)
public class Company {

    private UUID id;

    private String siret;
    private String name;
    private CompanyType type;
    private String address;

}
