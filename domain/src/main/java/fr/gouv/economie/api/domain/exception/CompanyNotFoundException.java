package fr.gouv.economie.api.domain.exception;

import java.util.UUID;

public class CompanyNotFoundException extends RuntimeException {

    public CompanyNotFoundException(UUID companyId) {
        super("Company with id: " + companyId + " is not found");
    }
}
