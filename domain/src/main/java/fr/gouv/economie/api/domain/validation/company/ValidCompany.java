package fr.gouv.economie.api.domain.validation.company;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidCompanyValidator.class)
@Documented
public @interface ValidCompany {

    String message() default "must be a valid company according to the type";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
