package fr.gouv.economie.api.domain.service;

import fr.gouv.economie.api.domain.model.Company;
import fr.gouv.economie.api.domain.repository.CompanyRepository;
import lombok.AllArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
public class CompanyService {

    private final CompanyRepository companyRepository;

    public Company create(Company company) {
        return companyRepository.save(company);
    }

    public Company get(UUID companyId) {
        return companyRepository.get(companyId);
    }
}
