package fr.gouv.economie.api.domain.validation.siret;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @see "https://github.com/EsupPortail/esup-pstagedata/blob/07648edc1cdb629bc4aa5e1de397e45790242dfa/esup-pstagedata-utils/src/main/java/org/esupportail/pstagedata/utils/Utils.java#L199"
 */
public class SiretValidator implements ConstraintValidator<Siret, String> {

    @Override
    public boolean isValid(String numSiret, ConstraintValidatorContext context) {
        if (numSiret.length() != 14 || !isNumber(numSiret)) return false;

        int total = 0;
        int nb;

        /* Cas de la Poste, nouvel algo en 2012 car plus de 10000 établissement, nombre limite de l'algo de Luhn **/
        if ("356000000".equals(numSiret.substring(0, 9))) {
            for (int i = 0; i < numSiret.length(); i++) {
                /* somme simple des chiffres du SIRET */
                nb = convertStringToInt(String.valueOf(numSiret.charAt(i)));
                total += nb;
            }
            boolean ret = !isNumber(numSiret) || Long.parseLong(numSiret) != 0;
            /* Si la somme est un multiple de 5 alors le SIRET de la Poste est valide */
            return total % 5 == 0 && ret;
        }
        /* Cas classique **/
        for (int i = 0; i < numSiret.length(); i++) {
            /* Recherche les positions impaires : 1er, 3e, 5e, etc... que l'on multiplie par 2*/
            if ((i % 2) == 0) {
                nb = convertStringToInt(String.valueOf(numSiret.charAt(i))) * 2;
                /* si le résultat est >9 alors il est composé de deux chiffres et ne pouvant être >19 le calcule devient : 1 + (nb -10) ou : nb - 9 */
                if (nb > 9) nb -= 9;
            } else {
                nb = convertStringToInt(String.valueOf(numSiret.charAt(i)));
            }
            total += nb;
        }
        boolean ret = !isNumber(numSiret) || Long.parseLong(numSiret) != 0;
        /* Si la somme est un multiple de 10 alors le SIRET est valide */
        return total % 10 == 0 && ret;
    }

    private boolean isNumber(String nb) {
        try {
            Long.parseLong(nb);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    private int convertStringToInt(String s) {
        int r = 0;
        try {
            r = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            //nothing
        }
        return r;
    }

}
