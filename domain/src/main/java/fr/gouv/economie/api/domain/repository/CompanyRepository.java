package fr.gouv.economie.api.domain.repository;

import fr.gouv.economie.api.domain.model.Company;

import java.util.UUID;

public interface CompanyRepository {

    Company save(Company company);

    Company get(UUID companyId);
}
