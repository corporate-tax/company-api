package fr.gouv.economie.api.domain.validation.company;

import fr.gouv.economie.api.domain.model.Company;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidCompanyValidator implements ConstraintValidator<ValidCompany, Company> {

    public boolean isValid(Company company, ConstraintValidatorContext context) {
        String address = company.getAddress();
        return company.getType().hasAddress() ? StringUtils.isNoneBlank(address) : address == null;
    }
}
