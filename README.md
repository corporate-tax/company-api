# company-api

## Introduction

This project provides REST webservices for managing companies' data.

## REST API

Cf. `https://{domain}/v3/api-docs/`

## Security

For the moment, the API is secured by a basic authentication:

```
username: user
password: Q!J=6xud_LVb2za=QNwP!yzURcRR8YnpM&Nt_@BGN4SwS%6Kt2mJe3LnA55AXJJg
```