package fr.gouv.economie.company.api.application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.gouv.economie.api.domain.exception.CompanyNotFoundException;
import fr.gouv.economie.api.domain.model.Company;
import fr.gouv.economie.api.domain.model.CompanyType;
import fr.gouv.economie.api.domain.service.CompanyService;
import fr.gouv.economie.company.api.application.common.error.ErrorResponse;
import fr.gouv.economie.company.api.application.dto.CompanyDto;
import fr.gouv.economie.company.api.application.dto.CreateCompanyDto;
import fr.gouv.economie.company.api.application.mapper.CompanyDtoMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CompanyController.class)
class CompanyControllerTest {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CompanyService companyService;

    @Test
    @WithMockUser("55c87815-000f-4f7c-8fd1-f6baab08d0a8")
    void create() throws Exception {
        CreateCompanyDto request = new CreateCompanyDto()
                .setName("TestCompany")
                .setSiret("87849547957650")
                .setType("AUTO_ENTREPRISE");

        Company company = CompanyDtoMapper.INSTANCE.toCompany(request).setId(UUID.randomUUID());
        when(companyService.create(any())).thenReturn(company);

        CompanyDto response = CompanyDtoMapper.INSTANCE.toCompanyDto(company);

        mockMvc.perform(post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                )
                .andExpect(status().isCreated())
                .andExpect(content().string((is(objectMapper.writeValueAsString(response)))));
    }

    @Test
    @WithMockUser("55c87815-000f-4f7c-8fd1-f6baab08d0a8")
    void get() throws Exception {
        Company company = new Company()
                .setId(UUID.randomUUID())
                .setName("TestCompany")
                .setSiret("87849547957650")
                .setType(CompanyType.AUTO_ENTREPRISE);

        when(companyService.get(any())).thenReturn(company);

        CompanyDto response = CompanyDtoMapper.INSTANCE.toCompanyDto(company);

        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{id}", company.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().string((is(objectMapper.writeValueAsString(response)))));
    }

    @Test
    @WithMockUser("55c87815-000f-4f7c-8fd1-f6baab08d0a8")
    void get_unknownCompany() throws Exception {
        UUID companyId = UUID.fromString("69a7ea4f-016c-40d8-9e4e-71e66d2709e2");
        CompanyNotFoundException exception = new CompanyNotFoundException(companyId);
        when(companyService.get(any())).thenThrow(exception);


        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{id}", companyId.toString()))
                .andExpect(status().isNotFound())
                .andExpect(content().string((is(objectMapper.writeValueAsString(new ErrorResponse(exception.getMessage()))))));
    }
}