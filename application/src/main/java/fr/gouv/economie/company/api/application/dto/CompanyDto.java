package fr.gouv.economie.company.api.application.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class CompanyDto extends CreateCompanyDto {

    private UUID id;

}
