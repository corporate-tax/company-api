package fr.gouv.economie.company.api.application.config;

import fr.gouv.economie.api.domain.repository.CompanyRepository;
import fr.gouv.economie.api.domain.service.CompanyService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public CompanyService companyService(CompanyRepository companyRepository) {
        return new CompanyService(companyRepository);
    }

}
