package fr.gouv.economie.company.api.application.dto;

import fr.gouv.economie.api.domain.validation.company.ValidCompany;
import fr.gouv.economie.api.domain.validation.siret.Siret;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Accessors(chain = true)
@ValidCompany
public class CreateCompanyDto {

    @Siret
    @NotNull
    private String siret;
    @NotBlank
    private String name;
    @NotNull
    private String type;

}
