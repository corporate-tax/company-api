package fr.gouv.economie.company.api.application.mapper;

import fr.gouv.economie.company.api.application.dto.CompanyDto;
import fr.gouv.economie.api.domain.model.Company;
import fr.gouv.economie.company.api.application.dto.CreateCompanyDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CompanyDtoMapper {

    CompanyDtoMapper INSTANCE = Mappers.getMapper(CompanyDtoMapper.class);

    Company toCompany(CreateCompanyDto companyDto);

    CompanyDto toCompanyDto(Company company);

}
