package fr.gouv.economie.company.api.application.controller;

import fr.gouv.economie.api.domain.exception.CompanyNotFoundException;
import fr.gouv.economie.api.domain.model.Company;
import fr.gouv.economie.api.domain.service.CompanyService;
import fr.gouv.economie.company.api.application.common.error.ErrorResponse;
import fr.gouv.economie.company.api.application.dto.CompanyDto;
import fr.gouv.economie.company.api.application.dto.CreateCompanyDto;
import fr.gouv.economie.company.api.application.mapper.CompanyDtoMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/companies")
@AllArgsConstructor
public class CompanyController {

    private final CompanyService companyService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyDto create(@RequestBody CreateCompanyDto createCompanyDto) {
        Company company = companyService.create(CompanyDtoMapper.INSTANCE.toCompany(createCompanyDto));
        return CompanyDtoMapper.INSTANCE.toCompanyDto(company);
    }

    @GetMapping("/{id}")
    public CompanyDto get(@PathVariable UUID id) {
        Company company = companyService.get(id);
        return CompanyDtoMapper.INSTANCE.toCompanyDto(company);
    }

    @ExceptionHandler(CompanyNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleCompanyNotFoundException(CompanyNotFoundException exception) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ErrorResponse(exception.getMessage()));
    }

}
