package fr.gouv.economie.company.api.infrastructure.dao;

import fr.gouv.economie.api.domain.exception.CompanyNotFoundException;
import fr.gouv.economie.api.domain.model.Company;
import fr.gouv.economie.api.domain.model.CompanyType;
import fr.gouv.economie.company.api.infrastructure.TestApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest(classes = TestApplication.class)
class JpaRepositoryCompanyDaoTest {

    @Autowired
    private JpaRepositoryCompanyDao jpaRepositoryCompanyDao;

    @Test
    void save() {
        Company company = new Company();
        company.setName("TestCompany");
        company.setSiret("63631197585512");
        company.setType(CompanyType.AUTO_ENTREPRISE);

        Company savedCompany = jpaRepositoryCompanyDao.save(company);

        assertThat(savedCompany.getId()).isNotNull();
    }

    @Test
    void get() {
        UUID companyId = UUID.fromString("6cb32d7f-3886-42f7-b8ce-7ca0c0b331f1");

        Company company = jpaRepositoryCompanyDao.get(companyId);

        Company expected = new Company()
                .setId(companyId)
                .setSiret("44372668776223")
                .setName("HyperCompany")
                .setType(CompanyType.AUTO_ENTREPRISE);

        assertThat(company).isEqualTo(expected);
    }

    @Test
    void get_notFound() {
        UUID companyId = UUID.fromString("6cb32d7f-3886-42f7-b8ce-7ca0c0b331f2");

        assertThatThrownBy(() -> jpaRepositoryCompanyDao.get(companyId))
                .isInstanceOf(CompanyNotFoundException.class)
                .hasMessage(new CompanyNotFoundException(companyId).getMessage());
    }
}