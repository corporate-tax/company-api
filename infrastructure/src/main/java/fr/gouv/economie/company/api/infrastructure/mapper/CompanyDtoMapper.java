package fr.gouv.economie.company.api.infrastructure.mapper;

import fr.gouv.economie.company.api.infrastructure.entity.CompanyEntity;
import fr.gouv.economie.api.domain.model.Company;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CompanyDtoMapper {

    CompanyDtoMapper INSTANCE = Mappers.getMapper(CompanyDtoMapper.class);

    Company toCompany(CompanyEntity companyEntity);

    CompanyEntity toCompanyEntity(Company company);

}
