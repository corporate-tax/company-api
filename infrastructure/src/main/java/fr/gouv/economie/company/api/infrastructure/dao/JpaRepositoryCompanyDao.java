package fr.gouv.economie.company.api.infrastructure.dao;

import fr.gouv.economie.api.domain.exception.CompanyNotFoundException;
import fr.gouv.economie.api.domain.model.Company;
import fr.gouv.economie.api.domain.repository.CompanyRepository;
import fr.gouv.economie.company.api.infrastructure.entity.CompanyEntity;
import fr.gouv.economie.company.api.infrastructure.mapper.CompanyDtoMapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface JpaRepositoryCompanyDao extends JpaRepository<CompanyEntity, UUID>, CompanyRepository {

    default Company save(Company company) {
        CompanyEntity companyEntity = CompanyDtoMapper.INSTANCE.toCompanyEntity(company);
        CompanyEntity savedEntity = save(companyEntity);
        return CompanyDtoMapper.INSTANCE.toCompany(savedEntity);
    }

    default Company get(UUID companyId) {
        CompanyEntity companyEntity = findById(companyId).orElseThrow(() -> new CompanyNotFoundException(companyId));
        return CompanyDtoMapper.INSTANCE.toCompany(companyEntity);
    }
}
